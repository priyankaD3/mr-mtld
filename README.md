Marathi MTLD Tool ![v0.1](https://img.shields.io/badge/Version-0.1-success) ![Language: Python](https://img.shields.io/badge/Language-Python-yellow) ![Licenses: GPLv3, GFDLv1.3](https://img.shields.io/badge/Licenses-GPLv3,_GFDLv1.3-blue)
---

The Marathi MTLD Tool has been developed as a part of M. Phil. research work submitted to
the Department of Linguistics, University of Mumbai
in 2021.

# Description

This tool calculates the number of repeated words present in
a Marathi text (written in the Devnagari script) by following
the Measure of Textual Lexical Diversity (MTLD)
approach. For further details of the MTLD approach, see 
McCarthy (2005)[^fn].

[^fn]: McCarthy, P. M. (2005). An assessment of the range
	   and usefulness of lexical diversity measures and the
       potential of the measure of textual, lexical
	   diversity (mtld) (Doctoral dissertation, The
	   University of Memphis).

As a step towards calculating the MTLD score of an input text, the
Marathi MTLD program performs a lemmatization
process (for more details see Dingankar (2021)[^fn1]), which is carried out in two
stages:

[^fn1]: Dingankar, P. (2021). Measure of Textual Lexical Diversity
(MTLD) Tool for Marathi (M. Phil. dissertation, Department of Linguistic, University of Mumbai).

1. _Lemmatization stage 1:_ Here, the words
   present in the input text are __stemmed__. The stemming operation is performed with the help of two dependencies - **indic_nlp_resource** and **indic_nlp_library**. The suffixes generated durring the stemming process are
   discarded with the support of **Exception list resource**. 

2. _Lemmatization stage 2:_ Here, base forms generated through the stemming process (stage 1) 
   are converted into their lemma forms with the help of **Lemma form library**.

Any Marathi text in the Devanagari script can be used as an input for the Marathi MTLD Tool. To learn about more features of the Marathi MTLD tool see [_Manual of Marathi MTLD Tool_](https://gitlab.com/priyankaD3/mr-mtld/-/blob/main/doc/mtld-doc.pdf).

# Dependencies

The two dependencies i.e. [__indic-nlp-library/indicnlp__](https://github.com/anoopkunchukuttan/indic_nlp_library)
and
[__indic-nlp-resources__](https://github.com/anoopkunchukuttan/indic_nlp_resources) used in the Marathi MTLD tool have been developed by Mr. Anoop Kunchukuttan. We are redistributing them in our project as the dependencies are
licensed under MIT license.

# Installation instructions

## For GNU/Linux:

1. Download the python from the PYTHON website
   (https://www.python.org/) and install it.

2. Install pip

   ```
   sudo apt-get install pip
   ```

3. Install PyQt5 designer tool

   ```
   sudo pip install PyQt5
   ```

4. Install morfessor

   ```
   sudo pip install morfessor
   ```

5. Install pandas

   ```
   sudo pip install pandas
   ```

6. Install docx2txt

   ```
   sudo pip install docx2txt
   ```

7. Install pyexcel_ods

   ```
   sudo pip install pyexcel_ods
   ```

8. Install openpyxl

   ```
   sudo pip install openpyxl
   ```

9. Download source code of Marathi MTLD tool project from
   https://gitlab.com/priyankaD3/mr-mtld.

10. Run the program

   ```
   python3 marathi-mtld-tool.py
   ```

## For Windows

1. Download the python from the PYTHON website
   (https://www.python.org/) and install it.

2. Install PyQt5 designer tool

   ```
   pip install PyQt5
   ```

3. Install morfessor

   ```
   pip install morfessor
   ```

4. Install pandas

   ```
   pip install pandas
   ```

5. Install docx2txt

   ```
   pip install docx2txt
   ```

6. Install pyexcel_ods

   ```
   pip install pyexcel-ods
   ```

7. Install openpyxl

   ```
   pip install openpyxl
   ```

8. Download source code of Marathi MTLD tool project from
   https://gitlab.com/priyankaD3/mr-mtld.

9. Run the program

   ```
   py marathi-mtld-tool.py

   ```

The executable file of the Marathi MTLD tool is available at
the following links:

For GNU/Linux:
https://sourceforge.net/projects/mr-mtld-linux/

For Windows:
https://sourceforge.net/projects/mr-mtld-windows/

# Acknowledgements

We are grateful to Prof. Kristopher Kyle for his [Python
package](https://github.com/kristopherkyle/lexical_diversity)
developed for calculating a variety of lexical diversity
indices. In Marathi MTLD tool, we have used a [piece of
code](https://gitlab.com/priyankaD3/mr-mtld/-/blob/be6abfbcfe035ef781e094b6b5515eccdcfd135c/marathi-mtld-tool.py#L568-611)
from this package. We are indebted to
[Niranjan](https://gitlab.com/niruvt) for (a) helping us make
this software suitable for the free software community (b)
suggestions related to the license-selection (c) designing the
directory structure of this repository and (d) helping in
the preparation of the user manual.

# Copyrights

© 2021 Priyanka Dingankar; Suhas Zimbar; Department of
Linguistics, University of Mumbai.

## Licenses

Runtime code: ![Runtime code](https://img.shields.io/badge/-GPLv3-purple)

Documentation: ![Documentation](https://img.shields.io/badge/-GFDLv1.3-orange)

#### Footnotes
