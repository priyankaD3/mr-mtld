# -*- coding: utf-8 -*-

import re
import sys
import os
from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import QMessageBox
sys.path.insert(0,'ext/indic-nlp-library')
from indicnlp import common

from indicnlp import unsupervised_morph
import pandas as pd
import os.path
import numpy as np
#import pymsgbox
import docx2txt
#import xlsxwriter
from pyexcel_ods import save_data
from collections import OrderedDict
from odf import text, teletype
from odf.opendocument import load

from datetime import datetime
#from pandas_ods_reader import read_ods

class Ui_MainWindow(object):
    
    
    def setupUi(self, MainWindow):
        try:
            self.MainWindow = MainWindow
            #print(path)
            MainWindow.setObjectName("MainWindow")
            MainWindow.resize(1250, 840)
            sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
            sizePolicy.setHorizontalStretch(0)
            sizePolicy.setVerticalStretch(0)
            sizePolicy.setHeightForWidth(MainWindow.sizePolicy().hasHeightForWidth())
            MainWindow.setSizePolicy(sizePolicy)
            MainWindow.setMinimumSize(QtCore.QSize(1260, 845))
            MainWindow.setMaximumSize(QtCore.QSize(1260, 845))
            font = QtGui.QFont()
            font.setPointSize(8)
            MainWindow.setFont(font)
            MainWindow.setAutoFillBackground(False)
            self.base_path="";
            try:
                if os.path.isfile(os.path.join(os.path.abspath("."), 'ext')):
                    self.base_path = os.path.abspath(".")
                else:
                    self.base_path = sys._MEIPASS                
            except Exception:
                self.base_path = os.path.abspath(".")            
            data_path= os.path.join(self.base_path, 'ext//style.qss')
            qss_file = open(data_path).read()
            MainWindow.setStyleSheet(qss_file)       
            self.centralwidget = QtWidgets.QWidget(MainWindow)
            self.centralwidget.setObjectName("centralwidget")
            self.btnUpload = QtWidgets.QPushButton(self.centralwidget)
            self.btnUpload.setGeometry(QtCore.QRect(1140, 140, 93, 81))
            font = QtGui.QFont()
            font.setPointSize(10)
            font.setBold(True)
            font.setWeight(75)
            self.btnUpload.setFont(font)
            self.btnUpload.setObjectName("btnUpload")
            self.tabWidgetOutput = QtWidgets.QTabWidget(self.centralwidget)
            self.tabWidgetOutput.setGeometry(QtCore.QRect(10, 500, 1101, 311))
            sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding)
            sizePolicy.setHorizontalStretch(0)
            sizePolicy.setVerticalStretch(0)
            sizePolicy.setHeightForWidth(self.tabWidgetOutput.sizePolicy().hasHeightForWidth())
            self.tabWidgetOutput.setSizePolicy(sizePolicy)
            font = QtGui.QFont()
            font.setPointSize(8)
            font.setBold(True)
            font.setWeight(75)
            self.tabWidgetOutput.setFont(font)
            self.tabWidgetOutput.setObjectName("tabWidgetOutput")
            self.tabOutput = QtWidgets.QWidget()
            self.tabOutput.setObjectName("tabOutput")
            self.txtOutput = QtWidgets.QPlainTextEdit(self.tabOutput)
            self.txtOutput.setEnabled(True)
            self.txtOutput.setGeometry(QtCore.QRect(0, 0, 1101, 281))
            sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding)
            sizePolicy.setHorizontalStretch(0)
            sizePolicy.setVerticalStretch(0)
            sizePolicy.setHeightForWidth(self.txtOutput.sizePolicy().hasHeightForWidth())
            self.txtOutput.setSizePolicy(sizePolicy)
            font = QtGui.QFont()
            font.setPointSize(12)
            self.txtOutput.setFont(font)
            self.txtOutput.setReadOnly(True)
            self.txtOutput.setObjectName("txtOutput")
            self.tabWidgetOutput.addTab(self.tabOutput, "")
            self.tabRemovedPunctuation = QtWidgets.QWidget()
            self.tabRemovedPunctuation.setObjectName("tabRemovedPunctuation")
            self.txtRemovedPunctuation = QtWidgets.QPlainTextEdit(self.tabRemovedPunctuation)
            self.txtRemovedPunctuation.setGeometry(QtCore.QRect(0, 0, 1121, 281))
            font = QtGui.QFont()
            font.setPointSize(15)
            self.txtRemovedPunctuation.setFont(font)
            self.txtRemovedPunctuation.setReadOnly(True)
            self.txtRemovedPunctuation.setObjectName("txtRemovedPunctuation")
            self.tabWidgetOutput.addTab(self.tabRemovedPunctuation, "")
            self.tabRemovedPunctuationwc = QtWidgets.QWidget()
            self.tabRemovedPunctuationwc.setObjectName("tabRemovedPunctuationwc")
            self.txtRemovedPunctuationwc = QtWidgets.QPlainTextEdit(self.tabRemovedPunctuationwc)
            self.txtRemovedPunctuationwc.setGeometry(QtCore.QRect(0, 0, 1121, 281))
            font = QtGui.QFont()
            font.setPointSize(15)
            self.txtRemovedPunctuationwc.setFont(font)
            self.txtRemovedPunctuationwc.setReadOnly(True)
            self.txtRemovedPunctuationwc.setObjectName("txtRemovedPunctuationwc")
            self.tabWidgetOutput.addTab(self.tabRemovedPunctuationwc, "")
            self.tabLemmaStage1 = QtWidgets.QWidget()
            self.tabLemmaStage1.setObjectName("tabLemmaStage1")
            self.txtLemmaStage1 = QtWidgets.QPlainTextEdit(self.tabLemmaStage1)
            self.txtLemmaStage1.setGeometry(QtCore.QRect(0, 0, 1121, 281))
            font = QtGui.QFont()
            font.setPointSize(15)
            self.txtLemmaStage1.setFont(font)
            self.txtLemmaStage1.setReadOnly(True)
            self.txtLemmaStage1.setObjectName("txtLemmaStage1")
            self.tabWidgetOutput.addTab(self.tabLemmaStage1, "")
            self.tabLemmaStage1wc = QtWidgets.QWidget()
            self.tabLemmaStage1wc.setObjectName("tabLemmaStage1wc")
            self.txtLemmaStage1wc = QtWidgets.QPlainTextEdit(self.tabLemmaStage1wc)
            self.txtLemmaStage1wc.setGeometry(QtCore.QRect(0, 0, 1121, 281))
            font = QtGui.QFont()
            font.setPointSize(15)
            self.txtLemmaStage1wc.setFont(font)
            self.txtLemmaStage1wc.setReadOnly(True)
            self.txtLemmaStage1wc.setObjectName("txtLemmaStage1wc")
            self.tabWidgetOutput.addTab(self.tabLemmaStage1wc, "")
            self.tabLemmaStage2 = QtWidgets.QWidget()
            self.tabLemmaStage2.setObjectName("tabLemmaStage2")
            self.txtLemmaStage2 = QtWidgets.QPlainTextEdit(self.tabLemmaStage2)
            self.txtLemmaStage2.setGeometry(QtCore.QRect(0, 0, 1121, 281))
            font = QtGui.QFont()
            font.setPointSize(15)
            self.txtLemmaStage2.setFont(font)
            self.txtLemmaStage2.setReadOnly(True)
            self.txtLemmaStage2.setObjectName("txtLemmaStage2")
            self.tabWidgetOutput.addTab(self.tabLemmaStage2, "")
            self.tabLemmaStage2wc = QtWidgets.QWidget()
            self.tabLemmaStage2wc.setObjectName("tabLemmaStage2wc")
            self.txtLemmaStage2wc = QtWidgets.QPlainTextEdit(self.tabLemmaStage2wc)
            self.txtLemmaStage2wc.setEnabled(True)
            self.txtLemmaStage2wc.setGeometry(QtCore.QRect(0, 0, 1121, 281))
            font = QtGui.QFont()
            font.setPointSize(15)
            self.txtLemmaStage2wc.setFont(font)
            self.txtLemmaStage2wc.setReadOnly(True)
            self.txtLemmaStage2wc.setObjectName("txtLemmaStage2wc")
            self.tabWidgetOutput.addTab(self.tabLemmaStage2wc, "")
            self.tabMTLD = QtWidgets.QWidget()
            self.tabMTLD.setObjectName("tabMTLD")
            self.txtMTLD = QtWidgets.QPlainTextEdit(self.tabMTLD)
            self.txtMTLD.setGeometry(QtCore.QRect(0, 0, 1121, 281))
            font = QtGui.QFont()
            font.setPointSize(15)
            self.txtMTLD.setFont(font)
            self.txtMTLD.setReadOnly(True)
            self.txtMTLD.setObjectName("txtMTLD")
            self.tabWidgetOutput.addTab(self.tabMTLD, "")
            self.tabWidgetInput = QtWidgets.QTabWidget(self.centralwidget)
            self.tabWidgetInput.setEnabled(True)
            self.tabWidgetInput.setGeometry(QtCore.QRect(10, 110, 1101, 311))
            font = QtGui.QFont()
            font.setPointSize(8)
            font.setBold(True)
            font.setWeight(75)
            self.tabWidgetInput.setFont(font)
            self.tabWidgetInput.setObjectName("tabWidgetInput")
            self.tabInput = QtWidgets.QWidget()
            self.tabInput.setObjectName("tabInput")
            self.txtInput = QtWidgets.QPlainTextEdit(self.tabInput)
            self.txtInput.setGeometry(QtCore.QRect(0, 0, 1101, 281))
            font = QtGui.QFont()
            font.setPointSize(15)
            font.setBold(False)
            font.setWeight(50)
            self.txtInput.setFont(font)
            self.txtInput.setPlainText("")        
            self.txtInput.setObjectName("txtInput")
            self.tabWidgetInput.addTab(self.tabInput, "")
            self.btnProcess = QtWidgets.QPushButton(self.centralwidget)
            self.btnProcess.setGeometry(QtCore.QRect(1140, 340, 93, 81))
            font = QtGui.QFont()
            font.setPointSize(10)
            font.setBold(True)
            font.setWeight(75)
            self.btnProcess.setFont(font)
            self.btnProcess.setObjectName("btnProcess")
            self.btnExportCSV = QtWidgets.QPushButton(self.centralwidget)
            self.btnExportCSV.setEnabled(False)
            self.btnExportCSV.setGeometry(QtCore.QRect(1140, 530, 93, 81))
            font = QtGui.QFont()
            font.setPointSize(10)
            font.setBold(True)
            font.setWeight(75)
            self.btnExportCSV.setFont(font)
            self.btnExportCSV.setObjectName("btnExportCSV")
            self.btnExportExcel = QtWidgets.QPushButton(self.centralwidget)
            self.btnExportExcel.setEnabled(False)
            self.btnExportExcel.setGeometry(QtCore.QRect(1140, 630, 93, 81))
            font = QtGui.QFont()
            font.setPointSize(10)
            font.setBold(True)
            font.setWeight(75)
            self.btnExportExcel.setFont(font)
            self.btnExportExcel.setObjectName("btnExportExcel")
            self.btnExportExportODS = QtWidgets.QPushButton(self.centralwidget)
            self.btnExportExportODS.setEnabled(False)
            self.btnExportExportODS.setGeometry(QtCore.QRect(1140, 727, 93, 81))
            font = QtGui.QFont()
            font.setPointSize(10)
            font.setBold(True)
            font.setWeight(75)
            self.btnExportExportODS.setFont(font)
            self.btnExportExportODS.setObjectName("btnExportExportODS")
            self.label = QtWidgets.QLabel(self.centralwidget)
            self.label.setGeometry(QtCore.QRect(500, 0, 731, 41))
            font = QtGui.QFont()
            font.setFamily("Arial Black")
            font.setBold(False)
            font.setItalic(False)
            font.setUnderline(False)
            font.setWeight(50)
            font.setStrikeOut(False)
            self.label.setFont(font)
            self.label.setObjectName("label")
            self.btnReset = QtWidgets.QPushButton(self.centralwidget)
            self.btnReset.setEnabled(True)
            self.btnReset.setGeometry(QtCore.QRect(1140, 240, 93, 81))
            font = QtGui.QFont()
            font.setPointSize(10)
            font.setBold(True)
            font.setWeight(75)
            self.btnReset.setFont(font)
            self.btnReset.setObjectName("btnReset")
            self.txtInputFileName = QtWidgets.QLineEdit(self.centralwidget)
            self.txtInputFileName.setGeometry(QtCore.QRect(280, 80, 691, 41))
            sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
            sizePolicy.setHorizontalStretch(0)
            sizePolicy.setVerticalStretch(0)
            sizePolicy.setHeightForWidth(self.txtInputFileName.sizePolicy().hasHeightForWidth())
            self.txtInputFileName.setSizePolicy(sizePolicy)
            font = QtGui.QFont()
            font.setPointSize(13)
            self.txtInputFileName.setFont(font)
            #self.txtInputFileName.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
            #self.txtInputFileName.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
            #self.txtInputFileName.setCenterOnScroll(False)
            self.txtInputFileName.setObjectName("txtInputFileName")
            self.lblOutputFileName = QtWidgets.QLabel(self.centralwidget)
            self.lblOutputFileName.setGeometry(QtCore.QRect(10, 440, 261, 31))
            sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
            sizePolicy.setHorizontalStretch(0)
            sizePolicy.setVerticalStretch(0)
            sizePolicy.setHeightForWidth(self.lblOutputFileName.sizePolicy().hasHeightForWidth())
            self.lblOutputFileName.setSizePolicy(sizePolicy)
            font = QtGui.QFont()
            font.setPointSize(10)
            self.lblOutputFileName.setFont(font)
            self.lblOutputFileName.setObjectName("lblOutputFileName")
            self.lblInputFileName = QtWidgets.QLabel(self.centralwidget)
            self.lblInputFileName.setGeometry(QtCore.QRect(10, 80, 251, 31))
            sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
            sizePolicy.setHorizontalStretch(0)
            sizePolicy.setVerticalStretch(0)
            sizePolicy.setHeightForWidth(self.lblInputFileName.sizePolicy().hasHeightForWidth())
            self.lblInputFileName.setSizePolicy(sizePolicy)
            font = QtGui.QFont()
            font.setPointSize(10)
            self.lblInputFileName.setFont(font)
            self.lblInputFileName.setObjectName("lblInputFileName")
            self.txtOutputFileName = QtWidgets.QLineEdit(self.centralwidget)
            self.txtOutputFileName.setGeometry(QtCore.QRect(280, 440, 691, 41))
            #self.txtOutputFileName.setReadOnly(True)
            sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
            sizePolicy.setHorizontalStretch(0)
            sizePolicy.setVerticalStretch(0)
            sizePolicy.setHeightForWidth(self.txtOutputFileName.sizePolicy().hasHeightForWidth())
            self.txtOutputFileName.setSizePolicy(sizePolicy)
            font = QtGui.QFont()
            font.setPointSize(10)
            self.txtOutputFileName.setFont(font)
            #self.txtOutputFileName.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
            #self.txtOutputFileName.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
            #self.txtOutputFileName.setCenterOnScroll(False)
            self.txtOutputFileName.setObjectName("txtOutputFileName")
            self.lblInputFileName_Note = QtWidgets.QLabel(self.centralwidget)
            self.lblInputFileName_Note.setGeometry(QtCore.QRect(10, 40, 681, 31))
            sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
            sizePolicy.setHorizontalStretch(0)
            sizePolicy.setVerticalStretch(0)
            sizePolicy.setHeightForWidth(self.lblInputFileName_Note.sizePolicy().hasHeightForWidth())
            self.lblInputFileName_Note.setSizePolicy(sizePolicy)
            font = QtGui.QFont()
            font.setPointSize(10)
            self.lblInputFileName_Note.setFont(font)
            self.lblInputFileName_Note.setObjectName("lblInputFileName_Note")
            MainWindow.setCentralWidget(self.centralwidget)
            self.statusbar = QtWidgets.QStatusBar(MainWindow)
            self.statusbar.setObjectName("statusbar")
            MainWindow.setStatusBar(self.statusbar)

            self.btnUpload.setStyleSheet(''' font-size: 18px; ''')
            self.btnReset.setStyleSheet(''' font-size: 18px; ''')
            self.btnUpload.setStyleSheet(''' font-size: 18px; ''')
            self.btnProcess.setStyleSheet(''' font-size: 18px; ''')
            
            self.btnExportCSV.setStyleSheet(''' font-size: 18px; ''')
            self.btnExportExcel.setStyleSheet(''' font-size: 18px; ''')
            self.btnExportExportODS.setStyleSheet(''' font-size: 18px; ''')
            
            self.tabWidgetInput.setStyleSheet(''' font-size: 15px; ''')
            self.tabWidgetOutput.setStyleSheet(''' font-size: 15px; ''')

            self.txtInput.setStyleSheet(''' font-size: 24px; ''')
            self.txtOutput.setStyleSheet(''' font-size: 24px; ''')
            self.txtRemovedPunctuation.setStyleSheet(''' font-size: 24px; ''')
            self.txtRemovedPunctuationwc.setStyleSheet(''' font-size: 24px; ''')
            self.txtLemmaStage1.setStyleSheet(''' font-size: 24px; ''')
            self.txtLemmaStage1wc.setStyleSheet(''' font-size: 24px; ''')
            self.txtLemmaStage2.setStyleSheet(''' font-size: 24px; ''')
            self.txtLemmaStage2wc.setStyleSheet(''' font-size: 24px; ''')
            self.txtMTLD.setStyleSheet(''' font-size: 24px; ''')

            self.lblInputFileName.setStyleSheet(''' font-size: 18px; ''')
            self.lblOutputFileName.setStyleSheet(''' font-size: 18px; ''')
            self.lblInputFileName_Note.setStyleSheet(''' font-size: 18px; ''')

            self.txtInputFileName.setStyleSheet(''' font-size: 18px; ''')
            self.txtOutputFileName.setStyleSheet(''' font-size: 18px; ''')

            self.retranslateUi(MainWindow)
            self.tabWidgetOutput.setCurrentIndex(0)
            self.tabWidgetInput.setCurrentIndex(0)
            QtCore.QMetaObject.connectSlotsByName(MainWindow)
            MainWindow.setTabOrder(self.tabWidgetInput, self.txtInput)
            MainWindow.setTabOrder(self.txtInput, self.btnUpload)
            MainWindow.setTabOrder(self.btnUpload, self.btnReset)
            MainWindow.setTabOrder(self.btnReset, self.btnProcess)
            MainWindow.setTabOrder(self.btnProcess, self.tabWidgetOutput)
            MainWindow.setTabOrder(self.tabWidgetOutput, self.txtRemovedPunctuation)
            MainWindow.setTabOrder(self.txtRemovedPunctuation, self.btnExportCSV)
            MainWindow.setTabOrder(self.btnExportCSV, self.btnExportExcel)
            MainWindow.setTabOrder(self.btnExportExcel, self.btnExportExportODS)
            MainWindow.setTabOrder(self.btnExportExportODS, self.txtRemovedPunctuationwc)
            MainWindow.setTabOrder(self.txtRemovedPunctuationwc, self.txtLemmaStage1)
            MainWindow.setTabOrder(self.txtLemmaStage1, self.txtLemmaStage1wc)
            MainWindow.setTabOrder(self.txtLemmaStage1wc, self.txtLemmaStage2)
            MainWindow.setTabOrder(self.txtLemmaStage2, self.txtLemmaStage2wc)
            MainWindow.setTabOrder(self.txtLemmaStage2wc, self.txtMTLD)
            MainWindow.setTabOrder(self.txtMTLD, self.txtOutput)
            #global variables
            self.inputText="";
            self.removedPunctuation="";
            self.removedPunctuationwc="";
            self.lemmaStage1="";
            self.lemmaStage1wc="";
            self.lemmaStage2="";
            self.lemmaStage2wc="";
            self.mtldcount="";
        except Exception as e:
            print(str(e))

    def retranslateUi(self, MainWindow):
        try:
            _translate = QtCore.QCoreApplication.translate
            MainWindow.setWindowTitle(_translate("MainWindow", "Marathi MTLD Tool"))
            self.btnUpload.setText(_translate("MainWindow", "Upload"))
            self.tabWidgetOutput.setTabText(self.tabWidgetOutput.indexOf(self.tabOutput), _translate("MainWindow", "Input"))
            self.tabWidgetOutput.setTabText(self.tabWidgetOutput.indexOf(self.tabRemovedPunctuation), _translate("MainWindow", "Removed Punctuation"))
            self.tabWidgetOutput.setTabText(self.tabWidgetOutput.indexOf(self.tabRemovedPunctuationwc), _translate("MainWindow", "Word Count"))
            self.tabWidgetOutput.setTabText(self.tabWidgetOutput.indexOf(self.tabLemmaStage1), _translate("MainWindow", "Lemmatization Stage I"))
            self.tabWidgetOutput.setTabText(self.tabWidgetOutput.indexOf(self.tabLemmaStage1wc), _translate("MainWindow", "WordCount"))
            self.tabWidgetOutput.setTabText(self.tabWidgetOutput.indexOf(self.tabLemmaStage2), _translate("MainWindow", "Lemmatization Stage II"))
            self.tabWidgetOutput.setTabText(self.tabWidgetOutput.indexOf(self.tabLemmaStage2wc), _translate("MainWindow", "Word Count"))
            self.tabWidgetOutput.setTabText(self.tabWidgetOutput.indexOf(self.tabMTLD), _translate("MainWindow", "MTLD"))
            self.tabWidgetInput.setToolTip(_translate("MainWindow", "<html><head/><body><p>Input</p></body></html>"))
            self.tabWidgetInput.setTabText(self.tabWidgetInput.indexOf(self.tabInput), _translate("MainWindow", "Input"))
            self.btnProcess.setText(_translate("MainWindow", "Process"))
            self.btnExportCSV.setText(_translate("MainWindow", "Export\n"
    "CSV"))
            self.btnExportExcel.setText(_translate("MainWindow", "Export \n"
    "Excel"))
            self.btnExportExportODS.setText(_translate("MainWindow", "Export \n"
    "ODS"))
            self.label.setText(_translate("MainWindow", "<html><head/><body><p><span style=\" font-size:24px; font-weight:600; color:#ff7373;\">Marathi MTLD Tool</span></p></body></html>"))
            self.btnReset.setText(_translate("MainWindow", "Clear"))
            self.lblOutputFileName.setText(_translate("MainWindow", "<html><head/><body><p><span style=\" font-weight:600; color:#000000;\">Generated output file name</span></p></body></html>"))
            self.lblInputFileName.setText(_translate("MainWindow", "<html><head/><body><p><span style=\" font-weight:600; color:#000000;\">Selected input file name</span></p></body></html>"))
            
            self.lblInputFileName_Note.setText(_translate("MainWindow", "<html><head/><body><p><span style=\" font-weight:600; color:#000000;\">Upload pre-existing file or type/paste input text below :</span></p></body></html>"))
            self.btnProcess.clicked.connect(self.btnProcessClick)
            self.btnReset.clicked.connect(self.btnResetClick)
            self.btnUpload.clicked.connect(self.uploadData)
            self.btnExportCSV.clicked.connect(self.ExportData)
            self.btnExportExcel.clicked.connect(self.ExportData)
            self.btnExportExportODS.clicked.connect(self.ExportData)
            self.txtInputFileName.setVisible(False)
            self.txtOutputFileName.setVisible(False)
            self.lblInputFileName.setVisible(False)
            self.lblOutputFileName.setVisible(False)
        except Exception as e:
            print(str(e))
         
        

    def preInitApp(self, MainWindow):
        try:            
            INDIC_NLP_RESOURCES= os.path.join(self.base_path, 'ext')
                        
            #sys.path.append('{}/src'.format(INDIC_NLP_LIB_HOME))        
            self.punctuation = "`` '' ' . , ? ! ) ( % / - # $ * & [ ] { } + ~ ^ < > _ -LRB- -RRB- SYM ; \" :".split(" ")
            print("Program loading.....");
            self.initApp(INDIC_NLP_RESOURCES);
        except Exception as e:
            print(str(e))
        
    def initApp(self,INDIC_RESOURCES_PATH):
        print("Please Wait...........")
        try:
            common.INDIC_RESOURCES_PATH=INDIC_RESOURCES_PATH 
            self.analyzer=unsupervised_morph.UnsupervisedMorphAnalyzer('mr')        
            
            self.array1={}
            csvFileVerbs=os.path.join(self.base_path,"ext//lemma-form-library-csv.csv");
            odsFileVerbs=os.path.join(self.base_path,"ext//lemma-form-library-ods.ods");
            excelFileVerbs=os.path.join(self.base_path,"ext//lemma-form-library-excel.xlsx");
            
            if os.path.isfile(csvFileVerbs):
                excelData=pd.read_csv(csvFileVerbs, sep=',',encoding='utf8')
            elif os.path.isfile(odsFileVerbs):
                excelData=pd.read_excel(odsFileVerbs)
            elif os.path.isfile(excelFileVerbs):
                excelData=pd.read_excel(excelFileVerbs)
            else:
                excelData=pd.read_csv(csvFileVerbs, sep=',',encoding='utf8')
            
            excelData.replace(np.nan, '', regex=True)
            for i in np.array(excelData):
                for j in i[1:]:
                    if(j!='' and i[0]!='' and j!=i[0]):
                        self.array1.update({j: i[0]})
               
            print("Application Started Sucessfully.")
        except Exception as e:
            print(str(e))
        
    def btnProcessClick(self):
        if self.txtInput.toPlainText()=="":
            self.ShowMessage("Please Enter Input Text", 'Message')
        else:
            self.btnProcess.setEnabled(False);
            self.fnProcessData()
            self.setOutputData()
            self.btnProcess.setEnabled(True);
            self.tabWidgetOutput.setCurrentIndex(0)
            self.btnExportCSV.setEnabled(True);
            self.btnExportExcel.setEnabled(True);
            self.btnExportExportODS.setEnabled(True);
            self.ShowMessage("Process Completed Successfully", 'Message')

    def btnResetClick(self):
        try:
            self.inputText="";
            self.removedPunctuation="";
            self.removedPunctuationwc="";
            self.lemmaStage1="";
            self.lemmaStage1wc="";
            self.lemmaStage2="";
            self.lemmaStage2wc="";
            self.mtldcount="";
            self.plainText="";
            self.txtInput.setPlainText("")
            self.setOutputData()
            self.tabWidgetOutput.setCurrentIndex(0)
            self.txtInputFileName.setText("")
            self.txtOutputFileName.setText("")
            self.btnExportCSV.setEnabled(False)
            self.btnExportExcel.setEnabled(False)
            self.btnExportExportODS.setEnabled(False)
            self.txtInputFileName.setVisible(False)
            self.txtOutputFileName.setVisible(False)
            self.lblInputFileName.setVisible(False)
            self.lblOutputFileName.setVisible(False)            
            
        except Exception as e:
            print(str(e))

    def ShowMessage(self,message,title):
        msgBox = QMessageBox()
        msgBox.setIcon(QMessageBox.Information)
        msgBox.setWindowTitle(title);
        msgBox.setText(message);
        msgBox.setWindowFlags(QtCore.Qt.Dialog | QtCore.Qt.CustomizeWindowHint | QtCore.Qt.WindowTitleHint | QtCore.Qt.WindowCloseButtonHint);
        msgBox.exec();
            
    def fnProcessData(self):
        try:
            self.inputText="";
            self.removedPunctuation="";
            self.removedPunctuationwc="";
            self.lemmaStage1="";
            self.lemmaStage1wc="";
            self.lemmaStage2="";
            self.lemmaStage2wc="";
            self.mtldcount="";
            self.plainText="";

            #Set Input Text
            self.inputText=self.txtInput.toPlainText().strip()   #inputText
                        
            self.plainText=self.tokenize(self.inputText)                     
            
            #Tokenized & reomved pumtuation Marks
            dummyStr = " "            
            self.removedPunctuation=dummyStr.join(self.plainText)     #removedPunctuation
            
            #count of tokenized & reomved pumtuation Marks data
            self.removedPunctuationwc=str(len(self.plainText))        #removedPunctuationwc
            
            #apply stemer 
            analyzes_tokens=self.analyzer.morph_analyze_document(self.plainText)            
            self.lemmaStage1=str(analyzes_tokens)                #lemmaStage1
            
            #word count applied stemer 
            self.lemmaStage1wc=str(len(analyzes_tokens))         #lemmaStage1wc

           
            finalData=self.replace_matched_items(analyzes_tokens, self.array1)
            self.lemmaStage2=str(finalData)                      #lemmaStage2
            self.lemmaStage2wc=str(len(finalData))               #lemmaStage2wc
            
            mtldFinal=self.mtld(finalData)
            self.mtldcount=str(mtldFinal)                        #mtldcount
            
           
        except Exception as e:
            #self.ShowMessage(str(e), 'Message')
            print(str(e))
        
    def setOutputData(self):
        try:
            self.txtOutput.setPlainText(self.inputText)
            self.txtRemovedPunctuation.setPlainText(self.removedPunctuation)
            self.txtRemovedPunctuationwc.setPlainText(self.removedPunctuationwc)
            self.txtLemmaStage1.setPlainText(self.lemmaStage1)
            self.txtLemmaStage1wc.setPlainText(self.lemmaStage1wc)
            self.txtLemmaStage2.setPlainText(self.lemmaStage2)
            self.txtLemmaStage2wc.setPlainText(self.lemmaStage2wc)
            self.txtMTLD.setPlainText(self.mtldcount)
        except Exception as e:
            print(str(e))
        
        
    def replace_matched_items(self,word_list, dictionary):
        new_list = [dictionary.get(item2, item2) for item2 in word_list]
        new_list1=[i.split(' ', 1) for i in new_list]
        new_list=(str(new_list1).replace('[','').replace(']','')).replace(' ','').replace('\'','').split(',')
        return new_list

    def mtld(self,input, min = 0): #original MTLD described in Jarvis & McCarthy         
        input_reversed = list(reversed(input))
        mtld_full = self.safe_divide((self.mtlder(input)+self.mtlder(input_reversed)),2)
        return mtld_full

    def mtlder(self,text):
            factor = 0
            factor_lengths = 0
            start = 0
            for x in range(len(text)):
                factor_text = text[start:x+1]                
                if x+1 == len(text):                    
                    factor += self.safe_divide((1 - self.ttr(factor_text)),(1 - .72))
                    factor_lengths += len(factor_text)            
                else:
                    if self.ttr(factor_text) < .720 and len(factor_text) >= 0:
                        factor += 1
                        factor_lengths += len(factor_text)
                        start = x+1
                    else:
                        continue
            mtld = self.safe_divide(factor_lengths,factor)
            return mtld
            
    def safe_divide(self, numerator, denominator):
        if denominator == 0 or denominator == 0.0:
            index = 0
        else:
            index = numerator/denominator
        return index
    
    def ttr(self, text):
        ntokens = len(text)
        ntypes = len(set(text))	
        return self.safe_divide(ntypes,ntokens)

    def tokenize(self,raw_text):        
        for x in self.punctuation:
            raw_text = raw_text.replace(x,"")
        raw_text = re.sub('\s+',' ',raw_text)
        raw_text = raw_text.lower()
        text = raw_text.split(" ")
        text1= [i for i in text if i]
        return text1

    def uploadData(self):
        try:
            #self.openFileNamesDialog()
            self.openMutipleFileNamesDialog()
            #uploadedData = docx2txt.process("Input.docx")
            #self.txtInput.(str(uploadedData))

        except Exception as e:
            print(str(e))
            
    def openMutipleFileNamesDialog(self):
        try:
            
            dialog = QtWidgets.QFileDialog()
            filename, _ = dialog.getOpenFileNames(None, "Upload File", "", "files (*.docx *.txt *.doc *.odt);;Word files (*.docx);;Text files (*.txt);;Word files (*.doc);;ODT document (*.odt)")
             
            if(len(filename)==1):
                self.txtInputFileName.setText(str(os.path.basename(os.path.splitext(filename[0])[0])))
                
                if(os.path.splitext(filename[0])[1]).lower()==".txt":
                    txtf = open(filename[0], 'r',encoding="utf8")                                
                    with txtf:
                        txtUploaddata = txtf.read()
                        self.txtInput.setPlainText(str(txtUploaddata))
                elif(os.path.splitext(filename[0])[1]).lower()==".docx" or (os.path.splitext(filename[0])[1]).lower()==".doc":
                    uploadedData = docx2txt.process(filename[0])
                    self.txtInput.setPlainText(str(uploadedData))
                elif(os.path.splitext(filename[0])[1]).lower()==".odt":
                    textdocodt = load(filename[0])
                    odfData=''
                    for odtText in textdocodt.getElementsByType(text.P):
                        odfData+=teletype.extractText(odtText)                
                    self.txtInput.setPlainText(str(odfData))

                self.txtInputFileName.setVisible(True)
                self.lblInputFileName.setVisible(True)
                self.ShowMessage("File Uploaded Successfully", 'Message')
                
            
            
            #print(filename)
            
            #uploadedData = docx2txt.process(filename)
            #self.txtInput.setText(str(uploadedData))          
            
            
        except Exception as e:
            print(str(e))
            self.ShowMessage(str(e), 'Error')
            
    def ExportData(self):
        try:
            
            
            exportMethod=self.MainWindow.sender().objectName();

            if(self.txtInputFileName.text().replace(" ","")!="" and exportMethod!="btnExportCSV"):
                filenameTime=self.txtInputFileName.text()+"_"+"Result_"+datetime.now().strftime("%Y_%m_%d_%I_%M_%S_%p")
            else :
                filenameTime= "Result_"+datetime.now().strftime("%Y_%m_%d_%I_%M_%S_%p")

            self.btnExportCSV.setEnabled(False);
            self.btnExportExcel.setEnabled(False);
            self.btnExportExportODS.setEnabled(False);
                
            fileExtension=""
            if(exportMethod=="btnExportExcel"):
                fileExtension=".xlsx"
            elif(exportMethod=="btnExportCSV"):
                fileExtension=".csv"
            elif(exportMethod=="btnExportExportODS"):
                fileExtension=".ods"
            
                            
            xlsf1 = pd.DataFrame( #[['BriefResult_'+filenameTime+'.xlsx', 'Word Count of Input Data','Number of Types','Number of Tokens','MTLD']],
               columns=['File Name', 'Word Count of Input Data','Number of Types','Number of Tokens','MTLD'])
                       
                           
            xlsf2 = pd.DataFrame(#[['ElaboratedResult'+filenameTime+'.xlsx' 'd']],
              columns=['File Name', 'Input Data','Word Count of Input Data','Lemmatized Data','Word Count After Lemmatization','Type','Number of Types','Tokens','Number of Tokens','Final MTLD'])
            
            row = 1
            xlsf2.loc[row, 'File Name'] = str(filenameTime+''+fileExtension);
            xlsf1.loc[row, 'File Name'] = str(filenameTime+''+fileExtension);
            
            for x in self.inputText.strip().split(' '):
                xlsf2.loc[row, 'Input Data'] = str(x)
                row+=1

             
            xlsf2.loc[1, 'Word Count of Input Data'] = str(row-1)#str(len(self.inputText.strip().split(' ')))
            xlsf1.loc[1, 'Word Count of Input Data'] = str(row-1)

            row = 1
            _data=list(self.lemmaStage2.strip().strip('][').split(', '))
            for x in _data:
                xlsf2.loc[row, 'Lemmatized Data'] = x.strip('\'')
                xlsf2.loc[row, 'Tokens'] = x.strip('\'')
                row+=1
                
            
            xlsf2.loc[1, 'Word Count After Lemmatization'] = str(row-1)               
            xlsf2.loc[1, 'Number of Tokens'] = str(row-1)
            xlsf1.loc[1, 'Number of Tokens'] = str(row-1)
            
            row = 1
            _data=sorted(set(_data), key=_data.index)
            for x in _data:
                xlsf2.loc[row, 'Type'] = x.strip('\'')
                row+=1
            xlsf2.loc[1, 'Number of Types'] = str(row-1)
            xlsf1.loc[1, 'Number of Types'] = str(row-1)
             
           
            xlsf2.loc[1, 'Final MTLD'] = self.mtldcount
            xlsf1.loc[1, 'MTLD'] = self.mtldcount

            if fileExtension==".csv":
                #with pd.ExcelWriter("result-files/CSV/Brief result"+filenameTime+'.csv') as writer:
                    if not os.path.exists("result-files//csv"):
                        os.makedirs("result-files//csv")
                    if(self.txtInputFileName.text().replace(" ","")!=""):
                        xlsf1.to_csv("result-files//csv//"+self.txtInputFileName.text()+"_"+"brief_"+filenameTime+'.csv', index=False,encoding ='utf-8-sig')
                        xlsf2.to_csv("result-files//csv//"+self.txtInputFileName.text()+"_"+"elaborated_"+filenameTime+'.csv', index=False,encoding ='utf-8-sig')
                        self.txtOutputFileName.setText(str(self.txtInputFileName.text()+"_"+"brief_"+filenameTime+";"+self.txtInputFileName.text()+"_"+"elaborated_"+filenameTime))
                        self.txtOutputFileName.setCursorPosition(0)
                    else:
                        xlsf1.to_csv("result-files//csv//brief_"+filenameTime+'.csv', index=False,encoding ='utf-8-sig')
                        xlsf2.to_csv("result-files//csv//elaborated_"+filenameTime+'.csv', index=False,encoding ='utf-8-sig')
                        self.txtOutputFileName.setText(str("brief_"+filenameTime+";"+"elaborated_"+filenameTime))
                        self.txtOutputFileName.setCursorPosition(0)
                #with pd.ExcelWriter("result-files/CSV/Elaboratedresult"+filenameTime+'.csv') as writer:
                  #  xlsf2.to_csv(writer, index=False)
            elif fileExtension==".xlsx":
                if not os.path.exists("result-files//Excel"):
                    os.makedirs("result-files//Excel")
                self.txtOutputFileName.setText(str(filenameTime))
                self.txtOutputFileName.setCursorPosition(0)
                with pd.ExcelWriter("result-files//Excel//"+filenameTime+'.xlsx') as writer:
                    xlsf1.to_excel(writer, sheet_name='Brief result',index=False)
                    xlsf2.to_excel(writer, sheet_name='Elaborated result',index=False)
            elif fileExtension==".ods":
                if not os.path.exists("result-files//ods"):
                    os.makedirs("result-files//ods")
                self.save_ods_from_excel(xlsf1,xlsf2, "result-files//ods//"+filenameTime+'.ods')
                self.txtOutputFileName.setText(str(filenameTime))
                self.txtOutputFileName.setCursorPosition(0)

            

            self.btnExportCSV.setEnabled(True);
            self.btnExportExcel.setEnabled(True);
            self.btnExportExportODS.setEnabled(True);

            
            self.txtOutputFileName.setVisible(True)                
            self.lblOutputFileName.setVisible(True)
            self.ShowMessage("File Generated and Saved Successfully", 'Message')
            #if (exportMethod=="btnExportExportODS")#btnExportCSV
                
        except Exception as e:
            print("error:"+str(e))

    def save_ods_from_excel(self,sheet1df,sheet2df, target_ods_file):
        try:

            sheet2df=sheet2df.fillna("")
            # Change everything to string since we're just writing
            sheet1df = sheet1df.astype(str)
            sheet2df = sheet2df.astype(str)

            # Initiliaze the empty dict() for the data
            d = OrderedDict()
            
            # Initiliaze data to be written as an empty list, as pyods needs a list to write
            whole_data_list = []
            
            # Write the columns first to be the first entries 
            whole_data_list.append(list(sheet1df.columns))
            
            # loop through data frame and update the data list
            for index, row in sheet1df.iterrows():
                whole_data_list.append(list(row.values))
                
            whole_data_list2 = []
            whole_data_list2.append(list(sheet2df.columns))
            for index, row in sheet2df.iterrows():                 
                whole_data_list2.append(list(row.values))
          
            # Populate dict() with updated data list
            
            d.update({"Brief result": whole_data_list})
            d.update({"Elaborated result": whole_data_list2})
            # Finally call save_data() from pyods to store the ods file
            save_data(target_ods_file, d)
        except Exception as e:
                print("error:"+str(e))
            
#Application Start
if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.show();    
    ui.preInitApp(MainWindow)
    sys.exit(app.exec_())

